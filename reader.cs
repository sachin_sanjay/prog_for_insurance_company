using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Linq;

namespace reader
{

    class program
    {
        static string getfilename(string a)
        {
            var b = a.ToCharArray();
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] == ((char)92))
                {
                    b[i] = ',';
                }
                else
                {
                    b[i] = a[i];
                }
            }
            string c = new string(b);
            string[] test = c.Split(',');
            string fname = test[test.Length - 1];
            return (fname);
        }
        public void byclaims(string cla, string i, string o)
        {
            string inputpath = i;
            string outputpath = o;
            string[] claimids = cla.Split(',');
            string[] allfiles = Directory.GetFiles(inputpath, "*.xml");
            string str="The Following Claim ID(s) could not be found:";
            List<string> lst = new List<string>(claimids);
            foreach (string c in allfiles)
            {
               var temp=lst.Except(cid(claimids, c, outputpath));
                lst = temp.ToList();
            }
            foreach(string nf in lst)
            {
                str += "\n" + nf;
            }
            MessageBox.Show(str);

        }
        static List<string> cid(string[] claimids, string inputpath, string outputpath)
        {
            int count = 0;
            List<string> identified = new List<string>();
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            string ID;
            XmlNode element1;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (string claimid in claimids)
            {
                foreach (XmlNode node in root)
                {
                    if (node.Name == "Header")
                    {
                        header = ddoc.ImportNode(node, true);
                    }
                    if (node.Name == "Claim")
                    {
                        ID = node.SelectSingleNode("ID").InnerText;
                        if (ID == claimid)
                        {
                            identified.Add(claimid);
                            if (count == 0)
                            {
                                element1 = ddoc.ImportNode(node, true);
                                ddoc.AppendChild(start);
                                for (int i = 0; i < count_att_rootnode; i += 1)
                                {
                                    rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                                }
                                ddoc.AppendChild(rootelement);
                                rootelement.AppendChild(header);
                                rootelement.AppendChild(element1);
                                count++;
                              
                            }
                            //                  outputpath+=@"\"+claimid+".xml";
                            // ddoc.Save(outputpath);
                            else
                            {
                                element1 = ddoc.ImportNode(node, true);
                                rootelement.AppendChild(element1);
                            }
                        }
                    }
                }
            }
            if (count > 0)
            {
                outputpath += @"\" + getfilename(inputpath);
                ddoc.Save(outputpath);
                

            }
            return (identified);
        }
        public void bydenials(string i, string o)
        {
            string inputpath = i;//2
            string outputpath = o;//3
            string[] allfiles = Directory.GetFiles(inputpath, "*.xml");
            foreach (string c in allfiles)
            {
                den(c, outputpath);

            }
        }
        public void onlysid(string i,string o, string sid)
        {
            string[] allfiles = Directory.GetFiles(i, "*.xml");
            foreach (string c in allfiles)
            {
                denwitsid(c, o, sid);

            }
            
        }

        public void onlydate(string i, string o, int date)
        {
            string[] allfiles = Directory.GetFiles(i, "*.xml");
            foreach (string c in allfiles)
            {
                denwitdate(c, o, date);

            }

        }
        public void onlydenandsid(string i, string o, string dencode,string sid)
        {
            string[] allfiles = Directory.GetFiles(i, "*.xml");
            foreach (string c in allfiles)
            {
                denwitcodeandsid(c, o, dencode,sid);

            }

        }
        public void onlydenandmonth(string i, string o, string dencode,int mon)
        {
            string[] allfiles = Directory.GetFiles(i, "*.xml");
            foreach (string c in allfiles)
            {
                denwitcodeandmonth(c, o, dencode, mon);

            }
        }
        public void onlydateandsid(string i, string o, string date, int mon)
        {
            string[] allfiles = Directory.GetFiles(i, "*.xml");
            foreach (string c in allfiles)
            {
                denwitdateandsid(c, o, mon,date);

            }
        }
        public void bydenials(string i, string o,string dcode)
        {
            string inputpath = i;//2
            string outputpath = o;//3
            string[] allfiles = Directory.GetFiles(inputpath, "*.xml");
            foreach (string c in allfiles)
            {
                denwitcode(c, outputpath,dcode);

            }
        }
        public void bydenials(string i, string o, string dcode,string sid, int mon)
        {
            string inputpath = i;//2
            string outputpath = o;//3
            string[] allfiles = Directory.GetFiles(inputpath, "*.xml");
            foreach (string c in allfiles)
            {
                denwitcode(c, outputpath, dcode,sid,mon);

            }
        }
        static void den(string inputpath, string outputpath)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                }
                if (node.Name == "Claim")
                {
                    string payment, netamount, DenialCode;
                    int pay, net;
                    try
                    {
                        DenialCode = node.SelectSingleNode("Activity/DenialCode").InnerText;
                    }
                    catch
                    {
                        DenialCode = "";
                    }
                    try
                    {
                        payment = node.SelectSingleNode("Activity/PaymentAmount").InnerText;
                        netamount = node.SelectSingleNode("Activity/Net").InnerText;
                    }
                    catch
                    {
                        payment = "-7777";
                        netamount = "-7777";
                    }
                    string ID = node.SelectSingleNode("ID").InnerText;
                    try
                    {
                        pay = Int32.Parse(payment);
                    }
                    catch
                    {
                        float payf = float.Parse(payment);
                        pay = Convert.ToInt32(Math.Ceiling(payf));
                    }
                    try
                    {
                        net = Int32.Parse(netamount);
                    }
                    catch
                    {
                        float netf = float.Parse(netamount);
                        net = Convert.ToInt32(Math.Ceiling(netf));
                    }
                    if ((DenialCode.Length > 2) || (pay == 0) || (pay != net))
                    {
                        if (count == 0)
                        {
                            XmlNode element1 = ddoc.ImportNode(node, true);
                            ddoc.AppendChild(start);
                            for (int i = 0; i < count_att_rootnode; i += 1)
                            {
                                rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                            }
                            ddoc.AppendChild(rootelement);
                            rootelement.AppendChild(header);
                            rootelement.AppendChild(element1);
                            count++;
                        }
                        else
                        {
                            XmlNode element1 = ddoc.ImportNode(node, true);
                            rootelement.AppendChild(element1);

                        }
                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }
        static void denwitcode(string inputpath, string outputpath,string dencode)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                }
                if (node.Name == "Claim")
                {
                    string DenialCode="";
                    foreach (XmlNode act in node)
                    {
                        try
                        {
                            if (act.Name == "Activity")
                                DenialCode = act.SelectSingleNode("DenialCode").InnerText;

                            if (DenialCode == dencode)
                            {
                                if (count == 0)
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    ddoc.AppendChild(start);
                                    for (int i = 0; i < count_att_rootnode; i += 1)
                                    {
                                        rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                                    }
                                    ddoc.AppendChild(rootelement);
                                    rootelement.AppendChild(header);
                                    rootelement.AppendChild(element1);
                                    count++;
                                }
                                else
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    rootelement.AppendChild(element1);

                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }
        static void denwitcode(string inputpath, string outputpath, string dencode, string sid, int mon)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            string senderid, tdate;
            int mth;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                    senderid = node.SelectSingleNode("SenderID").InnerText;
                    tdate = node.SelectSingleNode("TransactionDate").InnerText;
                    string[] dates = tdate.Split('/');
                    mth = Int32.Parse(dates[1]);
                    if (!((senderid == sid) && (mth == mon)))
                    {
                        break;
                    }
                }
                if (node.Name == "Claim")
                {
                    string DenialCode = "";
                    foreach (XmlNode act in node)
                    {
                        try
                        {
                            if (act.Name == "Activity")
                                DenialCode = act.SelectSingleNode("DenialCode").InnerText;

                            if (DenialCode == dencode)
                            {
                                if (count == 0)
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    ddoc.AppendChild(start);
                                    for (int i = 0; i < count_att_rootnode; i += 1)
                                    {
                                        rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                                    }
                                    ddoc.AppendChild(rootelement);
                                    rootelement.AppendChild(header);
                                    rootelement.AppendChild(element1);
                                    count++;
                                }
                                else
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    rootelement.AppendChild(element1);

                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }
        static void denwitsid(string inputpath, string outputpath,string sid)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            string senderid;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                    senderid = node.SelectSingleNode("SenderID").InnerText;
                    if (!(senderid == sid))
                    {
                        break;
                    }
                }
                if (node.Name == "Claim")
                {
                    if (count == 0)
                    {
                        XmlNode element1 = ddoc.ImportNode(node, true);
                        ddoc.AppendChild(start);
                        for (int i = 0; i < count_att_rootnode; i += 1)
                        {
                            rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                        }
                        ddoc.AppendChild(rootelement);
                        rootelement.AppendChild(header);
                        rootelement.AppendChild(element1);
                        count++;
                    }
                    else
                    {
                        XmlNode element1 = ddoc.ImportNode(node, true);
                        rootelement.AppendChild(element1);

                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }
        static void denwitdate(string inputpath, string outputpath, int date)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            string senderid, tdate;
            int mth;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                    senderid = node.SelectSingleNode("SenderID").InnerText;
                    tdate = node.SelectSingleNode("TransactionDate").InnerText;
                    string[] dates = tdate.Split('/');
                    mth = Int32.Parse(dates[1]);
                    if (!(mth == date))
                    {
                        break;
                    }
                }
                if (node.Name == "Claim")
                {
                    if (count == 0)
                    {
                        XmlNode element1 = ddoc.ImportNode(node, true);
                        ddoc.AppendChild(start);
                        for (int i = 0; i < count_att_rootnode; i += 1)
                        {
                            rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                        }
                        ddoc.AppendChild(rootelement);
                        rootelement.AppendChild(header);
                        rootelement.AppendChild(element1);
                        count++;
                    }
                    else
                    {
                        XmlNode element1 = ddoc.ImportNode(node, true);
                        rootelement.AppendChild(element1);

                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }

        }
        static void denwitdateandsid(string inputpath, string outputpath, int date,string sid)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            string senderid, tdate;
            int mth;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                    senderid = node.SelectSingleNode("SenderID").InnerText;
                    tdate = node.SelectSingleNode("TransactionDate").InnerText;
                    string[] dates = tdate.Split('/');
                    mth = Int32.Parse(dates[1]);
                    if (!((sid==senderid)&&(mth == date)))
                    {
                        break;
                    }
                }
                if (node.Name == "Claim")
                {
                    if (count == 0)
                    {
                        XmlNode element1 = ddoc.ImportNode(node, true);
                        ddoc.AppendChild(start);
                        for (int i = 0; i < count_att_rootnode; i += 1)
                        {
                            rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                        }
                        ddoc.AppendChild(rootelement);
                        rootelement.AppendChild(header);
                        rootelement.AppendChild(element1);
                        count++;
                    }
                    else
                    {
                        XmlNode element1 = ddoc.ImportNode(node, true);
                        rootelement.AppendChild(element1);

                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }
        static void denwitcodeandsid(string inputpath, string outputpath, string dencode, string sid)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            string senderid;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                    senderid = node.SelectSingleNode("SenderID").InnerText;
                    if (!(senderid == sid))
                    {
                        break;
                    }
                }
                if (node.Name == "Claim")
                {
                    string DenialCode = "";
                    foreach (XmlNode act in node)
                    {
                        try
                        {
                            if (act.Name == "Activity")
                                DenialCode = act.SelectSingleNode("DenialCode").InnerText;

                            if (DenialCode == dencode)
                            {
                                if (count == 0)
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    ddoc.AppendChild(start);
                                    for (int i = 0; i < count_att_rootnode; i += 1)
                                    {
                                        rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                                    }
                                    ddoc.AppendChild(rootelement);
                                    rootelement.AppendChild(header);
                                    rootelement.AppendChild(element1);
                                    count++;
                                }
                                else
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    rootelement.AppendChild(element1);

                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }
        static void denwitcodeandmonth(string inputpath, string outputpath, string dencode, int mon)
        {
            XmlDocument doc = new XmlDocument();
            XmlDocument ddoc = new XmlDocument();
            XmlNode start = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlNode header = ddoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.Load(inputpath);
            XmlNode root = doc.DocumentElement;
            string rootname = root.Name;
            int count_att_rootnode = root.Attributes.Count;
            int count = 0;
            string tdate;
            int mth;
            XmlElement rootelement = ddoc.CreateElement(rootname);
            foreach (XmlNode node in root)
            {
                if (node.Name == "Header")
                {
                    header = ddoc.ImportNode(node, true);
                    tdate = node.SelectSingleNode("TransactionDate").InnerText;
                    string[] dates = tdate.Split('/');
                    mth = Int32.Parse(dates[1]);
                    if (!(mth == mon))
                    {
                        break;
                    }
                }
                if (node.Name == "Claim")
                {
                    string DenialCode = "";
                    foreach (XmlNode act in node)
                    {
                        try
                        {
                            if (act.Name == "Activity")
                                DenialCode = act.SelectSingleNode("DenialCode").InnerText;

                            if (DenialCode == dencode)
                            {
                                if (count == 0)
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    ddoc.AppendChild(start);
                                    for (int i = 0; i < count_att_rootnode; i += 1)
                                    {
                                        rootelement.SetAttribute(root.Attributes[i].Name, root.Attributes[i].Value);
                                    }
                                    ddoc.AppendChild(rootelement);
                                    rootelement.AppendChild(header);
                                    rootelement.AppendChild(element1);
                                    count++;
                                }
                                else
                                {
                                    XmlNode element1 = ddoc.ImportNode(node, true);
                                    rootelement.AppendChild(element1);

                                }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            if (count > 0)
            {
                string opath = outputpath + @"\" + getfilename(inputpath);
                ddoc.Save(opath);
            }


        }

    }

}